# Crypto Chart

An easy to use, responsive, and privacy-friendly site to convert popular cryptocurrencies to USD.

### Prerequisites

Need a web server (anything that can run HTML), or a browser (to view the files as the user would see them) and something to edit the html files in (two suggested editors are [Notepad++](https://notepad-plus-plus.org/), or [VSCodium](https://vscodium.com/)).

## Deployment

[Download the repo](https://bitbucket.org/Pronfu/cryptochart/downloads/), then unzip the folder into another, copy the contents of that folder into the folder that you are editing in, or the folder on your server (most likely public_html). Refresh and you should see it. 

## Live Demo

[https://projects.gregoryhammond.ca/cryptochart/](https://projects.gregoryhammond.ca/cryptochart/)

## License

This project is licensed under [Unlicense](https://unlicense.org/)

## Acknowledgments

Inspiration from this came from Rockecoon - Dashboard made by [Toglas Studio](https://dribbble.com/toglas), posted on [dribbble](https://dribbble.com/shots/9142192-Rockecoon-Dashboard)

Framework used: [Formantic-UI](https://fomantic-ui.com/) which is a community fork of [Semantic-UI](https://semantic-ui.com/) under [MIT license](https://tldrlegal.com/license/mit-license).